import {Component, OnInit} from '@angular/core';
import {TraccarService} from '../services/traccar/traccar.service';
import {Storage} from '@ionic/storage';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ScriptLoaderService} from 'angular-google-charts';
import TrackBeesUtils from '../utils/track-bees-utils';
import {Hive, Position} from '../model';
import {hivesKey, positionsKey} from '../shared/consts';
import {TranslateService} from '@ngx-translate/core';
import {LoadingController} from '@ionic/angular';

declare var google: any;

@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard-page.component.html',
    styleUrls: ['dashboard-page.component.scss']
})

export class DashboardPage implements OnInit {
    positions: Position[];
    hives: Hive[] = [];
    type = 'BarChart';
    humidityTitle: string;
    temperatureTitle: string;
    chartsDataTemp: any;
    chartsDataHumid: any;
    chartHeight: number;
    loadingMessage: string;
    options = {
        hAxis: {
            title: null,
            minValue: 0,
        },
        vAxis: {
            title: null,
            minValue: 0,
            gridlines: {
                count: 4
            }
        },
        legend: {
            position: 'none'
        },
        annotations: {
            alwaysOutside: false
        },
        chartArea: {
            height: '80%',
            backgroundColor: {
                fill: '#f4f5f8'
            },
        },
        width: '100%'
    };
    private unsubscriber$ = new Subject();
    private chartTableTemperature = [];
    private chartTableHumidity = [];

    constructor(
        private traccarService: TraccarService,
        private storage: Storage,
        private translateService: TranslateService,
        private loaderService: ScriptLoaderService,
        private loadCtrl: LoadingController) {
    }


    ngOnInit(): void {
        this.chartsDataTemp = [];
        this.chartsDataHumid = [];
        this.translateService
            .get(['HIVES_TEMPERATURE_TITLE', 'HIVES_HUMIDITY_TITLE'])
            .pipe(takeUntil(this.unsubscriber$))
            .subscribe(value => {
                if (value.HIVES_TEMPERATURE_TITLEL) {
                    this.temperatureTitle = value.HIVES_TEMPERATURE_TITLE;
                }
                if (value.HIVES_HUMIDITY_TITLE) {
                    this.humidityTitle = value.HIVES_HUMIDITY_TITLE;
                }
                if (value.LOADING_MESSAGE) {
                    this.loadingMessage = value.LOADING_MESSAGE;
                }
            });
        this.preloadHives();
    }

    ionViewDidLeave() {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    ionViewDidEnter() {
        this.traccarService.positionsGet().pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(positions => {
            this.storage.get(hivesKey).then((hives: Hive[]) => {
                positions.forEach(position => {
                    position.device = hives.find(hive => hive.id === position.deviceId);
                });
                this.positions = positions;
                this.storage.set(positionsKey, positions);
                this.loadCtrl.create({ message: this.loadingMessage})
                    .then(load => {
                        load.present();
                        this.prepareDrawingData(positions);
                        load.dismiss();
                    });
            });
        });
    }

    hasPositions(): boolean {
        return this.positions != null && this.positions.length > 0;
    }

    private getColorForTemp(temp: number): string {
        if (temp) {
            if (TrackBeesUtils.isTemperatureRed(temp)) {
                return '#DF5353';
            } else if (TrackBeesUtils.isTemperatureAmber(temp)) {
                return '#ffbf00';
            }
        }
        return '#55BF3B';
    }

    private getColorForHumidity(humidity: number): string {
        if (humidity) {
            if (TrackBeesUtils.isHumidityRed(humidity)) {
                return '#DF5353';
            } else if (TrackBeesUtils.isHumidityAmber(humidity)) {
                return '#ffbf00';
            }
        }
        return '#55BF3B';
    }

    private prepareDrawingData(positions: Position[]): void {
        if (this.hasPositions()) {
            this.chartTableTemperature = [
                ['Element', '', {role: 'style'}, {role: 'annotation'}],
            ];
            this.chartTableHumidity = [
                ['Element', '', {role: 'style'}, {role: 'annotation'}],
            ];
            positions.forEach(pos => {
                if (TrackBeesUtils.hasTemperature(pos)) {
                    const temp = pos.attributes.temperature;
                    this.chartTableTemperature.push([
                        pos.device.name, temp,
                        this.getColorForTemp(temp),
                        temp.toString().concat(' °C')
                    ]);
                }
                if (TrackBeesUtils.hasHumidity(pos)) {
                    const humidity = pos.attributes.humidity;
                    this.chartTableHumidity.push([
                        pos.device.name, humidity,
                        this.getColorForHumidity(humidity),
                        humidity.toString().concat(' %')
                    ]);
                }
            });
            this.loaderService.onReady.subscribe(() => {
                this.loaderService.loadChartPackages([this.type]).subscribe(() => {
                    const tempDataTable = google.visualization.arrayToDataTable(this.chartTableTemperature);
                    this.chartsDataTemp = tempDataTable;
                    this.chartsDataHumid = google.visualization.arrayToDataTable(this.chartTableHumidity);

                    // Get dynamic height
                    if (tempDataTable != null) {
                        this.chartHeight = tempDataTable.getNumberOfRows() * 32;
                    }
                });
            });
        }
    }

    private preloadHives() {
        this.traccarService.devicesGet().pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(hives => {
            this.hives = hives;
            this.storage.set(hivesKey, hives);
        });
    }
}
