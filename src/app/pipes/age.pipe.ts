import {Injectable, Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

/**
 * This pipe is without suffix "ago".
 */
@Pipe({
    name: 'age'
})
@Injectable()
export class AgePipe implements PipeTransform {

    transform(value: any, currentLanguage?: string) {
        if (!currentLanguage) {
            currentLanguage = 'en';
        }
        return moment(value).locale(currentLanguage).fromNow(true);
    }
}
