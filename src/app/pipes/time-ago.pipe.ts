import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'timeAgo',
    pure: false
})
export class TimeAgoPipe implements PipeTransform {
    transform(value: any, currentLanguage?: string) {
        if (!currentLanguage) {
            currentLanguage = 'en';
        }
        return moment(value).locale(currentLanguage).fromNow();
    }
}
