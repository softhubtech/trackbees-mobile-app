import {Injectable, OnInit} from '@angular/core';
import {HttpHeaders} from '@angular/common/http';
import {Hive, Position, User} from '../../model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClientBase} from '../http-client.base';
import {ApiError, isApiError} from '../../model/api-error.model';
import {userKey} from '../../shared/consts';
import {Storage} from '@ionic/storage';

const APIEndpoinst = {
    session: 'session',
    devices: 'devices',
    users: 'users',
    positions: 'positions'
};

interface PositionsQueryParams {
    id?: number;
    deviceId?: number;
    from?: string;
    to?: string;
}

@Injectable({
    providedIn: 'root'
})
export class TraccarService implements OnInit {

    constructor(private httpClientBase: HttpClientBase, private storage: Storage) {
    }

    ngOnInit(): void {
    }

    devicesGet(params?: {}, headers?: HttpHeaders): Observable<Hive[]> {
        return this.httpClientBase.get<Hive[]>(`${APIEndpoinst.devices}`, params, headers).pipe(
            map(response => {
                if (!isApiError(response)) {
                    return response;
                }
            })
        );
    }

    public devicesPost(hive: Hive, headers?: HttpHeaders): Observable<Hive | ApiError> {
        return this.httpClientBase.post<Hive, Hive>(`${APIEndpoinst.devices}`, hive, headers);
    }

    public oneDevicePut(hive: Hive, headers?: HttpHeaders): Observable<Hive | ApiError> {
        return this.httpClientBase.put<Hive, Hive>(`${APIEndpoinst.devices}/${hive.id}`, hive, headers);
    }

    public sessionPost(user: User): Observable<number> {
        if (user.email === null || user.email === undefined) {
            throw new Error('Required parameter email was null or undefined when calling sessionPost.');
        }
        if (user.password === null || user.password === undefined) {
            throw new Error('Required parameter password was null or undefined when calling sessionPost.');
        }

        const headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
        return this.httpClientBase.post<User, string>(`${APIEndpoinst.session}`,
            `email=${user.email}&password=${user.password}`, headers).pipe(
            map(response => {
                if (isApiError(response)) {
                    return response.status;
                } else {
                    response.password = user.password;
                    this.storage.set(userKey, response);
                    return 200;
                }
            })
        );
    }

    public usersPost(user: User): Observable<User | ApiError> {
        return this.httpClientBase.post<User, User>(`${APIEndpoinst.users}`, user).pipe(
            map(response => {
                if (!isApiError(response)) {
                    response.password = user.password;
                    this.storage.set(userKey, response);
                }
                return response;
            })
        );
    }

    public oneUserPut(user: User): Observable<User> {
        return this.httpClientBase.put<User, User>(`${APIEndpoinst.users}/${user.id}`,
            user).pipe(
            map(response => {
                if (!isApiError(response)) {
                    return response;
                }
            })
        );
    }

    public onePositionGet(hive: Hive): Observable<Position> {
        const params: PositionsQueryParams = {id: hive.positionId};
        return this.positionsGet(params).pipe(
            map(positions => positions.find((value, index) => index === 0))
        );
    }

    public devicePositions(hive: Hive, from: Date, to: Date): Observable<Position[]> {
        const params: PositionsQueryParams = {deviceId: hive.id, from: from.toISOString(), to: to.toISOString()};
        return this.positionsGet(params);
    }

    public positionsGet(params?: PositionsQueryParams): Observable<Position[]> {
        params = params !== undefined ? params : {};
        return this.httpClientBase.get<Position[]>(`${APIEndpoinst.positions}`, params).pipe(
            map(response => {
                if (!isApiError(response)) {
                    return response;
                }
            })
        );
    }
}
