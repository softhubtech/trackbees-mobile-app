import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Storage} from '@ionic/storage';
import {TraccarService} from '../traccar/traccar.service';
import {map, mergeMap, tap} from 'rxjs/operators';
import {fromPromise} from 'rxjs/internal-compatibility';
import {User} from '../../model';
import {Platform} from '@ionic/angular';
import {userKey} from '../../shared/consts';


@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    private authenticated: boolean;
    constructor(private platform: Platform,
                private storage: Storage,
                private traccarService: TraccarService) {
        this.platform.ready().then(() => {
        // Uncomment below line if you want to force log out
        // this.storage.remove(userKey);
        });

    }

    isUserAuthenticated(): Observable<boolean> {
        return fromPromise(this.storage.get(userKey)).pipe(
            mergeMap(user => {
                if (!user) {
                    return of(false);
                } else if (this.authenticated) {
                    return of(this.authenticated);
                } else  {
                    return this.authenticate(user).pipe(
                        map(responseStatusCode => responseStatusCode === 200)
                    );
                }
            })
        );
    }

    authenticate(user: User): Observable<number> {
        return this.traccarService.sessionPost(user).pipe(
            tap(responseStatusCode => {
                if (responseStatusCode === 200) {
                    this.authenticated = true;
                }
            })
        );
    }
}
