import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpRequest, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';

import {Observable, of} from 'rxjs';
import {map, catchError, retry, filter, switchMap} from 'rxjs/operators';
import {Storage} from '@ionic/storage';
import {Hive, User} from '../model';
import {fromPromise} from 'rxjs/internal-compatibility';
import {userKey} from '../shared/consts';
import {ApiError} from '../model/api-error.model';

enum HttpMethod {
    post = 'POST',
    get = 'GET',
    put = 'PUT',
    delete = 'DELETE'
}

const baseUrl = 'http://demo5.traccar.org/api';
// const baseUrl = 'http://192.168.8.102:8082/api';
// const baseUrl = 'http://192.168.10.141:8082/api';


@Injectable({
    providedIn: 'root'
})
export class HttpClientBase {
    private user: User = {email: '', password: ''};

    constructor(private httpClient: HttpClient, private storage: Storage) {
    }

    get<T>(relativeUrl: string, params?: {}, headers?: HttpHeaders): Observable<T | ApiError> {
        return this.handleRequest<T, void>(HttpMethod.get, relativeUrl, null, params, headers);

    }

    post<T, A>(relativeUrl: string, body: A, headers?: HttpHeaders): Observable<T | ApiError> {
        return this.handleRequest<T, A>(HttpMethod.post, relativeUrl, body, {}, headers);
    }

    put<T, A>(relativeUrl: string, body: A, headers?: HttpHeaders): Observable<T | ApiError> {
        return this.handleRequest<T, A>(HttpMethod.put, relativeUrl, body);
    }

    private handleRequest<T, A>(method: HttpMethod, relativeUrl: string, body?: A | null, params?: {},
                                headers?: HttpHeaders): Observable<T | ApiError> {

        if (!headers) {
            headers = new HttpHeaders();
        }
        return this.addDefaultHeaders(headers).pipe(
            switchMap((headersWithDefaultHeaders) => {
                let httpRequest;
                if (HttpMethod.get === method) {
                    httpRequest = new HttpRequest<A>(method, `${baseUrl}/${relativeUrl}`, {
                        params: new HttpParams({fromObject: params}),
                        headers: headersWithDefaultHeaders
                    });
                } else {
                    httpRequest = new HttpRequest<A>(method, `${baseUrl}/${relativeUrl}`, body, {headers: headersWithDefaultHeaders});
                }

                return this.httpClient.request<T>(httpRequest).pipe(
                    retry(0),
                    filter(value => value instanceof HttpResponse),
                    map((event: HttpResponse<T>) => {
                        return event.body;
                    }),
                    catchError((errorResponse: HttpErrorResponse) => {
                        return of<ApiError>({status: errorResponse.status});
                    }));
            })
        );
    }

    private addDefaultHeaders(headers: HttpHeaders): Observable<HttpHeaders> {
        return fromPromise(this.storage.get(userKey).then((user: User) => {
                if (user) {
                    this.user = user;
                }
                return headers.set('Authorization', 'Basic ' + btoa(this.user.email + ':' + this.user.password));
            })
        );
    }


}
