import {Component} from '@angular/core';
import {Storage} from '@ionic/storage';
import {User} from '../model';
import {userKey} from '../shared/consts';

@Component({
    selector: 'app-settings',
    templateUrl: 'settings-page.component.html',
    styleUrls: ['settings-page.component.scss']
})
export class SettingsPage {
    user: User;

    constructor(private storage: Storage) {
    }
    ionViewDidEnter() {
        this.getUserFromStorage();
    }
    private getUserFromStorage() {
        this.storage.get(userKey).then( (user: User) => {
            this.user = user;
        });
    }
}
