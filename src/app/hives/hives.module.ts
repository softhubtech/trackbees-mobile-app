import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {HivesPage} from './hives-page.component';
import {TranslateModule} from '@ngx-translate/core';
import {AddHivePage} from '../pages/add-hive/add-hive.page';
import {TimeAgoPipe} from '../pipes/time-ago.pipe';
import {HivesRoutingModule} from './hives-routing.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        HivesRoutingModule,
        TranslateModule.forChild()
    ],
    declarations: [HivesPage, AddHivePage, TimeAgoPipe],
    exports: [
        TimeAgoPipe
    ],
    entryComponents: [AddHivePage]
})
export class HivesPageModule {
}
