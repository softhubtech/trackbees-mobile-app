import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {HivesPage} from './hives-page.component';


const routes: Routes = [
    {
        path: '',
        component: HivesPage
    },
    {
        path: 'hive-details',
        loadChildren: () => import('../pages/hive-details/hive-details.module').then(m => m.HiveDetailsPageModule)
    },
    {
        path: 'hive-data-history',
        loadChildren: () => import('../pages/hive-data-history/hive-data-history.module').then(m => m.HiveDataHistoryPageModule)
    },
    {
        path: 'hive-details/hive-extra',
        loadChildren: () => import('../pages/hive-extra/hive-extra.module').then(m => m.HiveExtraPageModule)
    },
    {
        path: 'hive-details/hive-on-map',
        loadChildren: () => import('../pages/hive-on-map/hive-on-map.module').then(m => m.HiveOnMapPageModule)
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule
    ],
})
export class HivesRoutingModule {
}
