import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {IonicModule} from '@ionic/angular';

import {HivesPage} from './hives-page.component';

describe('Tab2Page', () => {
    let component: HivesPage;
    let fixture: ComponentFixture<HivesPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HivesPage],
            imports: [IonicModule.forRoot()]
        }).compileComponents();

        fixture = TestBed.createComponent(HivesPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
