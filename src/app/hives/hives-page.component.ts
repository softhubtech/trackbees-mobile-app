import {Component, OnDestroy, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {LoadingController, ModalController} from '@ionic/angular';
import {TraccarService} from '../services/traccar/traccar.service';
import {Subject} from 'rxjs';
import {startWith, takeUntil} from 'rxjs/operators';
import {AddHivePage} from '../pages/add-hive/add-hive.page';
import {Router} from '@angular/router';
import {Hive, Position} from '../model';
import {Storage} from '@ionic/storage';
import {hivesKey, positionsKey} from '../shared/consts';
import {OverlayEventDetail} from '@ionic/core';

@Component({
    selector: 'app-hives',
    templateUrl: 'hives-page.component.html',
    styleUrls: ['hives-page.component.scss']
})
export class HivesPage implements OnInit, OnDestroy {

    currentLanguage;
    hives: Hive[];
    positions: Position[] = [];
    deleteWaitString: string;
    hiveLoadString: string;
    loading: any;
    private unsubscriber$ = new Subject();

    constructor(private translateService: TranslateService, private traccarService: TraccarService,
                private loadingController: LoadingController, private modalController: ModalController,
                private router: Router, private storage: Storage) {
    }

    ionViewDidEnter() {
        // this.getPositionsFromStorage();
        // this.refreshHives();
    }

    async openHive(hive: Hive) {
        await this.getPositionsFromStorage();
        const position: Position = this.positions.find(value => value.deviceId === hive.id);
        this.router.navigate(['/tabs/hives/hive-details'], {state: {hive, position}});
    }

    async addHive() {
        const addHiveModal = await this.modalController.create({
            animated: true,
            component: AddHivePage,
            componentProps: {hives: this.hives}
        });

        addHiveModal.onDidDismiss().then((hives: OverlayEventDetail<Hive>) => {
            if (hives && hives.data) {
                const hive = hives.data;
                this.hives.push(hive);
            }
        });
        await addHiveModal.present();

    }

    removeHive(hive: Hive) {
        console.log('Remove hive clicked');
    }

    async refreshHives(event?) {
        let cachedHives: Hive[] = await this.storage.get(hivesKey);
        if (!cachedHives) {
            cachedHives = [];
        }
        this.traccarService.devicesGet().pipe(
            takeUntil(this.unsubscriber$),
            startWith(cachedHives)
        ).subscribe(hives => {
            this.hives = hives;
            this.storage.set(hivesKey, hives);
        }, () => {
        }, () => {
            if (event) {
                event.target.complete();
            }
        });

    }
    ngOnInit(): void {
        this.translateService.get(['WAIT_HIVE_DELETE_MESSAGE', 'WAIT_HIVE_LOAD_MESSAGE'])
            .pipe(takeUntil(this.unsubscriber$))
            .subscribe(values => {
                this.deleteWaitString = values.WAIT_HIVE_DELETE_MESSAGE;
                this.hiveLoadString = values.WAIT_HIVE_LOAD_MESSAGE;
            });
        this.currentLanguage = this.translateService.getDefaultLang();
        this.getPositionsFromStorage();
        this.refreshHives();
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    private getPositionsFromStorage() {
        return this.storage.get(positionsKey).then((positions: Position[]) => {
            if (positions && positions.length > 0) {
                this.positions = positions;
            }
        });
    }
}
