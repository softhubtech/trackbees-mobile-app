import {Position} from '../model/position.model';

export default class TrackBeesUtils {
    /**
     * Checks if temperature is red
     */
    public static isTemperatureRed(temperature: number): boolean {
        return temperature && (temperature > 40 || temperature < 27);
    }

    /**
     * Checks if temperature is amber
     */
    public static isTemperatureAmber(temperature: number): boolean {
        return temperature
            && (temperature >= 27 && temperature <= 36 || temperature >= 36 && temperature <= 40);
    }


    /**
     * Checks if humidity is red
     */
    public static isHumidityRed(humidity: number): boolean {
        return humidity && (humidity > 90 || humidity < 30);
    }

    /**
     * Checks if humidity is amber
     */
    public static isHumidityAmber(humidity: number): boolean {
        return humidity && (humidity >= 30 && humidity <= 50 || humidity >= 70 && humidity <= 90);
    }

    public static hasTemperature(position: Position): boolean {
        return this.hasAttributes(position)
            && position.attributes.temperature != null;
    }

    public static hasHumidity(position: Position): boolean {
        return this.hasAttributes(position)
            && position.attributes.humidity != null;
    }

    public static hasAttributes(position: Position): boolean {
        return position.attributes != null;
    }

    /**
     * Shortens the names to be displayed into the mobile screen
     */
    public static shortenName(name: string): string {
        if (name && name.length > 5) {
            return name.substr(0, 5);
        }
        return name;
    }
}
