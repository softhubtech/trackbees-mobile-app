export const userKey = 'user';
export const positionsKey = 'positions';
export const hivesKey = 'hives';
export const defaultHistoryDays = 30;
