export interface Hive {
    name: string;
    uniqueId: string;
    status?: Status;
    groupId?: number;
    positionId?: number;
    id?: number;
    phone?: number;
    attributes?: HiveAttributes;
    lastUpdate?: Date;
    description?: string;
}
interface HiveAttributes {
    queenInstalledDate?: Date;
    notes?: string;
}
enum Status {
    unknown = 'unknown',
    online = 'online',
    offline = 'offline',
}

