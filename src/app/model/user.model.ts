export interface User {
    id?: number;
    phone?: number;
    email: string;
    name?: string;
    password: string;
    attributes?: UserAttributes;
}
interface UserAttributes {
    notificationTokens: string;
}
