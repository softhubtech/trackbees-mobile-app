export interface ApiError {
  status: number;
}

export function isApiError(object: any): object is ApiError {
    return ('status' in object && Object.keys(object).length === 1);
}
