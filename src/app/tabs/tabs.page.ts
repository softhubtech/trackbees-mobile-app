import {Component, OnDestroy, OnInit} from '@angular/core';
import {NotificationService} from '../services/notification/notification.service';
import {switchMap, takeUntil, tap} from 'rxjs/operators';
import {AngularFirestore} from '@angular/fire/firestore';
import {Platform} from '@ionic/angular';
import {Subject} from 'rxjs';
import {FirebaseX} from '@ionic-native/firebase-x/ngx';

@Component({
    selector: 'app-tabs',
    templateUrl: 'tabs.page.html',
    styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit, OnDestroy {
    private unsubscriber$ = new Subject();

    constructor(private notificationService: NotificationService, private angularFirestore: AngularFirestore,
                private platform: Platform, private firebase: FirebaseX) {
    }

    ngOnInit(): void {
        this.handleTokenRefresh();
        this.saveTokenIfnotSaved();
    }

    private handleTokenRefresh() {
        this.firebase.onTokenRefresh().pipe(
            takeUntil(this.unsubscriber$),
            tap(x => {
                if (this.platform.is('ios')) {
                    this.firebase.grantPermission().then(() => {
                        this.saveTokenToFireStore(x);
                    });
                }
                if (this.platform.is('android')) {
                    this.saveTokenToFireStore(x);
                }
            }),
            switchMap(token => {
                return this.notificationService.registerToken(token);
            })
        ).subscribe();

    }

    saveTokenToFireStore(token: string) {
        if (!token) {
            return;
        }
        const devicesRef = this.angularFirestore.collection('devices');
        const docData = {
            token,
            userId: 'testUser'
        };
        console.log('Saving token: ' + token);
        return devicesRef.doc(token).set(docData);
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }


    private saveTokenIfnotSaved() {
        this.firebase.getToken().then(token => {
            if (token) {
                this.notificationService.registerToken(token).pipe(
                    takeUntil(this.unsubscriber$)
                ).subscribe();
            }
        });
    }
}
