import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HiveOnMapPage } from './hive-on-map.page';

describe('HiveOnMapPage', () => {
  let component: HiveOnMapPage;
  let fixture: ComponentFixture<HiveOnMapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HiveOnMapPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HiveOnMapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
