import {Component, OnInit} from '@angular/core';
import {Hive, Position} from '../../model';
import {Router} from '@angular/router';
import {LoadingController, Platform} from '@ionic/angular';
import {Environment, GoogleMap, GoogleMaps, GoogleMapsEvent, Marker} from '@ionic-native/google-maps/ngx';

@Component({
    selector: 'app-hive-on-map',
    templateUrl: './hive-on-map.page.html',
    styleUrls: ['./hive-on-map.page.scss'],
})
export class HiveOnMapPage implements OnInit {
    currentPosition: Position;
    hive: Hive = {name: '', uniqueId: '', attributes: {}};
    map: GoogleMap;
    loading: any;

    constructor(private router: Router,
                private platform: Platform,
                private loadCtrl: LoadingController) {
    }

    ngOnInit() {
        this.hive = this.router.getCurrentNavigation().extras.state.hive;
        this.currentPosition = this.router.getCurrentNavigation().extras.state.currentPosition;
        this.loadCtrl.create().then(load => {
                load.present();
                this.platform.ready();
                this.loadMap();
                load.dismiss();
            });
    }

    loadMap() {
        // This code is necessary for browser
        Environment.setEnv({
            API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyBFO_-35lGKcrLO4CVe31bsJ9dSqb9O7-A',
            API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyBFO_-35lGKcrLO4CVe31bsJ9dSqb9O7-A'
        });
        this.map = GoogleMaps.create('map_canvas', {
            camera: {
                target: {
                    lat: this.currentPosition.latitude,
                    lng: this.currentPosition.longitude
                },
                zoom: 18,
                tilt: 30
            }
        });
        const marker: Marker = this.map.addMarkerSync({
            title: this.hive.name,
            icon: '#b36200',
            animation: 'DROP',
            position: {
                lat: this.currentPosition.latitude,
                lng: this.currentPosition.longitude
            }
        });
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
            // alert('clicked');
        });
    }
}
