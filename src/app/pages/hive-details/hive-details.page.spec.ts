import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HiveDetailsPage} from './hive-details.page';

describe('HiveDetailsPage', () => {
    let component: HiveDetailsPage;
    let fixture: ComponentFixture<HiveDetailsPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HiveDetailsPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HiveDetailsPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
