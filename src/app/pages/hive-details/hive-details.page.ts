import {Component, OnDestroy, OnInit} from '@angular/core';
import {Hive, Position} from '../../model';
import {Router} from '@angular/router';
import {TraccarService} from '../../services/traccar/traccar.service';
import {Observable, Subject} from 'rxjs';
import * as Highcharts from 'highcharts';
import HighchartMore from 'highcharts/highcharts-more';
import HighchartSoligGauge from 'highcharts/modules/solid-gauge';
import {filter, map, startWith, takeUntil, tap} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {Storage} from '@ionic/storage';
import {positionsKey, defaultHistoryDays} from '../../shared/consts';

HighchartMore(Highcharts);
HighchartSoligGauge(Highcharts);


@Component({
    selector: 'app-hive-details',
    templateUrl: './hive-details.page.html',
    styleUrls: ['./hive-details.page.scss'],
})
export class HiveDetailsPage implements OnInit, OnDestroy {
    currentLanguage;
    lastNDays = defaultHistoryDays;
    hive: Hive = {name: '', uniqueId: '', attributes: {}};
    currentPosition: Position;
    position$: Observable<Position>;
    positions: Position[];
    temperatureLabel = 'T';
    humidityLabel = 'H';
    from = new Date();
    to = new Date();
    unsubscriber$ = new Subject();
    Highcharts: typeof Highcharts = Highcharts;
    chartConstructor = 'chart';
    temperatureOptions: Highcharts.Options;
    humidityOptions: Highcharts.Options;

    constructor(
        private router: Router,
        private traccarService: TraccarService,
        private translateService: TranslateService,
        private storage: Storage) {
    }

    private hasAttributes(position: Position): boolean {
        return position.attributes !== undefined;
    }

    private hasKey(position: Position, key: string): string {
        if (position.attributes) {
            return position.attributes[key];
        }
        return;
    }

    /**
     * Checks if temperature is red
     */
    public isTemperatureRed(position: Position): boolean {
        if (!this.hasAttributes(position)) {
            return;
        }
        const temperature = position.attributes.temperature;
        return temperature && (temperature > 40 || temperature < 27);
    }

    /**
     * Checks if temperature is amber
     */
    public isTemperatureAmber(position: Position): boolean {
        if (!this.hasAttributes(position)) {
            return;
        }
        const temperature = position.attributes.temperature;
        return temperature && (temperature >= 27 && temperature <= 36 || temperature >= 36 && temperature <= 40);
    }

    /**
     * Checks if humidity is red
     */
    public isHumidityRed(position: Position): boolean {
        if (!this.hasAttributes(position)) {
            return;
        }
        const humidity = position.attributes.humidity;
        return humidity && (humidity > 90 || humidity < 30);
    }

    /**
     * Checks if humidity is amber
     */
    public isHumidityAmber(position: Position): boolean {
        if (!this.hasAttributes(position)) {
            return;
        }
        const humidity = position.attributes.humidity;
        return humidity && (humidity >= 30 && humidity <= 50 || humidity >= 70 && humidity <= 90);
    }

    ngOnInit() {
        this.currentLanguage = this.translateService.getDefaultLang();
        this.hive = this.router.getCurrentNavigation().extras.state.hive;
        this.translateService.get(['TEMPERATURE_LABEL', 'HUMIDITY_LABEL']).pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(values => {
            if (values.TEMPERATURE_LABEL) {
                this.temperatureLabel = values.TEMPERATURE_LABEL;
            }
            if (values.HUMIDITY_LABEL) {
                this.humidityLabel = values.HUMIDITY_LABEL;
            }
        });

        // This preloads the device last N positions for better UX, this seems ugly but it serves the pupose for now
        const position: Position = this.router.getCurrentNavigation().extras.state.position;
        this.from.setDate(this.from.getDate() - this.lastNDays);
        this.position$ = this.traccarService.devicePositions(this.hive, this.from, this.to).pipe(
            takeUntil(this.unsubscriber$),
            tap(positions => {
                this.positions = positions;
            }),
            map(positions2 => positions2.find((value, index) => index === positions2.length - 1)),
            startWith(position),
            filter(position2 => position2 !== undefined),
            tap(position3 => {
                this.currentPosition = position3;
                this.updatePositionInStorage(position3);
                const currentTemperature: number = position3.attributes.temperature;
                const currentHumidity: number = position3.attributes.humidity;
                if (currentTemperature) {
                    // This is important because it trigger change detection
                    this.setTemperatureOptions(currentTemperature);
                } else {
                    this.setTemperatureOptions(0);
                }
                if (currentHumidity) {
                    // This is important because it trigger change detection
                    this.setHumidityOptions(currentHumidity);
                } else {
                    this.setHumidityOptions(0);
                }
            })
        );

    }

    viewHiveDataHistory() {
        this.router.navigate(['/tabs/hives/hive-data-history'],
            {state: {hive: this.hive, positions: this.positions}});
    }
    viewHiveExtras() {
        this.router.navigate(['/tabs/hives/hive-details/hive-extra'],
            {state: {hive: this.hive, positions: this.positions}});
    }

    viewOnMap() {
        this.router.navigate(['/tabs/hives/hive-details/hive-on-map'],
            {state: {hive: this.hive, currentPosition: this.currentPosition}});
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    private updatePositionInStorage(position: Position) {
        this.storage.get(positionsKey).then((positions: Position[]) => {
            if (positions && positions.length >= 0) {
                const index = positions.findIndex(value => value.deviceId === position.deviceId);
                if (index >= 0) {
                    positions[index] = position;
                    this.storage.set(positionsKey, positions);
                } else {
                    positions.push(position);
                    this.storage.set(positionsKey, positions);
                }
            } else if (positions === undefined) {
                this.storage.set(positionsKey, [position]);
            }
        });
    }

    setTemperatureOptions(temperature: number) {
        this.temperatureOptions = {
            chart: {
                plotBackgroundImage: null,
                plotBackgroundColor: null,
                plotShadow: false,
                plotBorderWidth: 0,
                borderRadius: 0,
                height: 160,
                width: 160,
                spacing: [0, 0, 0, 0],
                margin: [0, 0, 0, 0]
            },

            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            pane: {
                startAngle: -90,
                endAngle: 90,
                background: null
            },
            yAxis: {
                title: {
                    text: this.temperatureLabel,
                    x: 0,
                    y: 20
                },
                min: 0,
                max: 100,
                lineColor: '#339',
                tickColor: '#339',
                minorTickColor: '#339',
                offset: -13,
                lineWidth: 1,
                labels: {
                    enabled: false
                },
                tickLength: 10,
                minorTickLength: 5,
                endOnTick: false,
                plotBands: [
                    {
                        from: 31,
                        to: 36,
                        color: '#55BF3B' // green
                    },
                    {
                        from: 27,
                        to: 31,
                        color: '#ffbf00' // amber
                    },
                    {
                        from: 36,
                        to: 40,
                        color: '#ffbf00' // amber
                    },
                    {
                        from: 40,
                        to: 100,
                        color: '#DF5353' // red
                    },
                    {
                        from: 0,
                        to: 27,
                        color: '#DF5353' // red
                    }
                ]
            },
            series: [{
                data: [temperature],
                name: 'T',
                type: 'gauge',
                dial: {
                    backgroundColor: '#b36200',
                },
                pivot: {
                        radius: 5,
                        borderWidth: 1,
                        borderColor: 'gray',
                        backgroundColor: {
                            linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
                            stops: [
                                [0, 'white'],
                                [1, 'gray']
                            ]
                        }
                    },
                dataLabels: {
                    format: '{y}°C',
                    defer: true,
                    shadow: false,
                    y: 5,
                    borderWidth: 0,
                    useHTML: true,
                    padding: 10,
                    borderColor: null,
                    shape: '°C',
                    style : {
                        fontSize: '14px'
                    }
                },
                tooltip: {
                    valueSuffix: '°C'
                }
            }]
        };
    }

    setHumidityOptions(humidity: number) {
        this.humidityOptions = {
            chart: {
                plotBackgroundImage: null,
                plotShadow: false,
                plotBorderWidth: 0,
                plotBackgroundColor: null,
                borderRadius: 0,
                height: 160,
                width: 160,
                selectionMarkerFill: null,
                spacing: [0, 0, 0, 0],
                margin: [0, 0, 0, 0]

            },
            title: {
                text: ''
            },
            pane: {
                startAngle: -90,
                endAngle: 90,
                background: null
            },
            credits: {
                enabled: false
            },
            plotOptions: {},
            yAxis: {
                title: {
                    text: this.humidityLabel,
                    x: 0,
                    y: 20
                },
                min: 0,
                max: 100,
                lineColor: '#339',
                tickColor: '#339',
                minorTickColor: '#339',
                offset: -13,
                lineWidth: 1,
                labels: {
                    distance: -15,
                    enabled: false
                },
                tickLength: 10,
                minorTickLength: 5,
                endOnTick: false,
                plotBands: [
                    {
                        from: 50,
                        to: 70,
                        color: '#55BF3B' // green
                    },
                    {
                        from: 30,
                        to: 50,
                        color: '#ffbf00' // amber
                    },
                    {
                        from: 70,
                        to: 90,
                        color: '#ffbf00' // amber
                    },
                    {
                        from: 0,
                        to: 30,
                        color: '#DF5353' // red
                    },
                    {
                        from: 90,
                        to: 100,
                        color: '#DF5353' // red
                    }
                ]
            },
            series: [{
                name: 'H',
                type: 'gauge',
                data: [humidity],
                dataLabels: {
                    format: '{y}%',
                    defer: true,
                    y: 5,
                    borderWidth: 0,
                    useHTML: true,
                    borderColor: null,
                    padding: 10,
                    shape: '°C',
                    style : {
                        fontSize: '14px'
                    }
                },
                tooltip: {
                    valueSuffix: '%'
                },
                dial: {
                    backgroundColor: '#b36200',
                },
                pivot: {
                    radius: 5,
                    borderWidth: 1,
                    borderColor: 'gray',
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
                        stops: [
                            [0, 'white'],
                            [1, 'gray']
                        ]
                    }
                }
            }]
        };
    }
}
