import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {HiveDetailsPage} from './hive-details.page';
import {HighchartsChartModule} from 'highcharts-angular';
import {TranslateModule} from '@ngx-translate/core';
import {AgePipe} from '../../pipes/age.pipe';

const routes: Routes = [{
        path: '', component: HiveDetailsPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HighchartsChartModule,
        RouterModule.forChild(routes),
        TranslateModule
    ],
    declarations: [HiveDetailsPage, AgePipe]
})
export class HiveDetailsPageModule {
}
