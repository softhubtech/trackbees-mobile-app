import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {TraccarService} from '../../services/traccar/traccar.service';
import {Hive} from '../../model';
import {AlertController, LoadingController, ToastController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {isApiError} from '../../model/api-error.model';

@Component({
    selector: 'app-hive-extra',
    templateUrl: './hive-extra.page.html',
    styleUrls: ['./hive-extra.page.scss'],
})
export class HiveExtraPage implements OnInit, OnDestroy {
    hive: Hive = {name: '', uniqueId: '', attributes: {}};
    private updatingHiveWaitingMessage: string;
    private updatingHiveSuccessfulMessage: string;
    private unsubscriber$: Subject<void> = new Subject<void>();
    private updateHiveError: string;

    constructor(private router: Router, private traccarService: TraccarService, private toastController: ToastController,
                private loadingController: LoadingController, private translateService: TranslateService,
                private alertController: AlertController) {
    }


    ngOnInit() {
        this.translateService.get(['WAIT_UPDATE_HIVE_MESSAGE', 'UPDATE_HIVE_ERROR', 'UPDATE_HIVE_SUCCESSFULLY_MESSAGE']).pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(values => {
            this.updatingHiveWaitingMessage = values.WAIT_UPDATE_HIVE_MESSAGE;
            this.updateHiveError = values.UPDATE_HIVE_ERROR;
            this.updatingHiveSuccessfulMessage = values.UPDATE_HIVE_SUCCESSFULLY_MESSAGE;
        });

        this.hive = this.router.getCurrentNavigation().extras.state.hive;
    }

    updateHive() {
        this.showLoader();
        this.traccarService.oneDevicePut(this.hive).subscribe(hive => {
            this.loadingController.dismiss();
            if (!isApiError(hive)) {
                this.showToast();
            } else {
                this.showErrorAlert();
            }
        });
    }

    private showLoader() {
        this.loadingController.create({
            spinner: null,
            message: this.updatingHiveWaitingMessage,
            translucent: true,
            cssClass: 'custom-class-to-customize-spinner',
            animated: true,
            backdropDismiss: true
        }).then(loading => {
            loading.present();
        });
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    private showToast() {
        this.toastController.create({
            message: this.updatingHiveSuccessfulMessage,
            duration: 4500,
            position: 'top',
            buttons: ['Ok'],
            cssClass: 'custom-class-to-customize-toast'
        }).then(toast => {
            toast.present();
        });
    }

    private showErrorAlert() {
        this.alertController.create({
            message: 'Error',
            subHeader: this.updateHiveError,
            buttons: ['OK']
        }).then(alert => {
            alert.present();
        });
    }
}
