import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HiveExtraPage } from './hive-extra.page';

describe('HiveExtraPage', () => {
  let component: HiveExtraPage;
  let fixture: ComponentFixture<HiveExtraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HiveExtraPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HiveExtraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
