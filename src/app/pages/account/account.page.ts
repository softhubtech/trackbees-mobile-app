import {Component, OnInit} from '@angular/core';
import {AlertController, Platform} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {Storage} from '@ionic/storage';
import {Router} from '@angular/router';
import {NotificationService} from '../../services/notification/notification.service';

@Component({
    selector: 'app-account',
    templateUrl: './account.page.html',
    styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
    private logoutConfirmationMessage: string;
    private logoutCancel: string;
    private logoutConfirm: string;
    private logoutLabel: string;

    constructor(private alertController: AlertController, private translateService: TranslateService, private platform: Platform,
                private storage: Storage, private router: Router, private notificationService: NotificationService) {
    }

    ngOnInit() {
        this.translateService.get(['LOGOUT_CONFIRMATION_MESSAGE', 'CANCEL_BUTTON', 'YES_LABEL', 'LOGOUT_LABEL']).subscribe(value => {
            this.logoutConfirmationMessage = value.LOGOUT_CONFIRMATION_MESSAGE;
            this.logoutCancel = value.CANCEL_BUTTON;
            this.logoutConfirm = value.YES_LABEL;
            this.logoutLabel = value.LOGOUT_LABEL;
        });
    }

    logout() {
        this.alertController.create({
            header: `${this.logoutLabel}`,
            message: this.logoutConfirmationMessage,
            buttons: [
                {
                    role: 'cancel',
                    text: this.logoutCancel,
                },
                {
                    text: this.logoutConfirm,

                    handler: async () => {
                        await this.router.navigate(['/welcome']);
                        await this.storage.clear();
                        if (this.platform.is('cordova')) {
                            await this.notificationService.unregisterToken();
                        }
                    }
                }

            ]
        }).then(alert => alert.present());

    }
}
