import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../model';
import {TraccarService} from '../../services/traccar/traccar.service';
import {TranslateService} from '@ngx-translate/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AlertController, LoadingController, ToastController} from '@ionic/angular';
import {Router} from '@angular/router';
import {isApiError} from '../../model/api-error.model';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.page.html',
    styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit, OnDestroy {
    confirmedPasswordModel: string;
    user: User = {
        email: '',
        password: ''
    };
    private unsubscriber$ = new Subject();
    private registerUserWaitingMessage: string;
    private registerUserError: string;
    private registerUserSuccessfullyMessage: string;

    constructor(private traccarService: TraccarService, private translateService: TranslateService,
                private loadingController: LoadingController, private router: Router,
                private toastController: ToastController, private alertController: AlertController) {
    }

    ngOnInit() {
        this.translateService.get(['WAIT_REGISTER_USER_MESSAGE', 'REGISTER_USER_ERROR', 'REGISTER_USER_SUCCESSFULLY_MESSAGE']).pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(values => {
            this.registerUserSuccessfullyMessage = values.REGISTER_USER_SUCCESSFULLY_MESSAGE;
            this.registerUserWaitingMessage = values.WAIT_REGISTER_USER_MESSAGE;
            this.registerUserError = values.REGISTER_USER_ERROR;
        });
    }

    signup() {
        this.showLoader();
        this.traccarService.usersPost(this.user).pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(user => {
            if (!isApiError(user)) {
                this.router.navigate(['tabs/dashboard']).then(() => {
                    this.loadingController.dismiss();
                    this.showToast();
                });
            } else {
                this.alertController.create({
                    message: 'Error',
                    subHeader: this.registerUserError,
                    buttons: ['OK']
                }).then(alert => {
                    this.loadingController.dismiss();
                    alert.present();
                });
            }

        });
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    private showToast() {
        this.toastController.create({
            message: this.registerUserSuccessfullyMessage,
            duration: 3000,
            position: 'top',
            buttons: ['Ok'],
            cssClass: 'custom-class-to-customize-toast'
        }).then(toast => {
            toast.present();
        });
    }

    private showLoader() {
        this.loadingController.create({
            spinner: null,
            message: this.registerUserWaitingMessage,
            translucent: true,
            cssClass: 'custom-class-to-customize-spinner',
            animated: true,
            backdropDismiss: true
        }).then(loading => {
            loading.present();
        });
    }

}
