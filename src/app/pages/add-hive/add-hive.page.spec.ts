import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddHivePage } from './add-hive.page';

describe('AddHivePage', () => {
  let component: AddHivePage;
  let fixture: ComponentFixture<AddHivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddHivePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddHivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
