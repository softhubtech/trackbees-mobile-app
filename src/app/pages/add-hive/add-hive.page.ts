import {Component, OnDestroy, OnInit} from '@angular/core';
import {AlertController, LoadingController, ModalController, Platform} from '@ionic/angular';
import {Hive} from '../../model';
import {TraccarService} from '../../services/traccar/traccar.service';
import {Subject} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {takeUntil} from 'rxjs/operators';
import {isApiError} from '../../model/api-error.model';

@Component({
    selector: 'app-add-hive',
    templateUrl: './add-hive.page.html',
    styleUrls: ['./add-hive.page.scss'],
})
export class AddHivePage implements OnInit, OnDestroy {
    public isIOS = this.platform.is('ios');
    hive: Hive = {name: '', uniqueId: '', attributes: {}};
    addingHiveWaitingMessage: string;
    addHiveError: string;
    private unsubscriber$ = new Subject();

    constructor(private platform: Platform, private modalController: ModalController,
                private traccarService: TraccarService, private translateService: TranslateService,
                private alertController: AlertController, private loadingController: LoadingController) {
    }

    ngOnInit() {
        this.translateService.get(['WAIT_ADD_HIVE_MESSAGE', 'ADD_HIVE_ERROR']).pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(values => {
            this.addingHiveWaitingMessage = values.WAIT_ADD_HIVE_MESSAGE;
            this.addHiveError = values.ADD_HIVE_ERROR;
        });
    }

    cancel() {
        this.modalController.dismiss();
    }

    done() {
        this.showLoader();
        this.traccarService.devicesPost(this.hive).pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(addedHive => {
            if (!isApiError(addedHive)) {
                this.modalController.dismiss(addedHive);
            } else {
                this.modalController.dismiss();
                this.alertController.create({
                    message: 'Error',
                    subHeader: this.addHiveError,
                    buttons: ['OK']
                }).then(alert => alert.present());
            }
            this.loadingController.dismiss();
        });
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    private showLoader() {
        this.loadingController.create({
            spinner: null,
            message: this.addingHiveWaitingMessage,
            translucent: true,
            cssClass: 'custom-class-to-customize-spinner',
            animated: true,
            backdropDismiss: true
        }).then(loading => {
            loading.present();
        });
    }


}
