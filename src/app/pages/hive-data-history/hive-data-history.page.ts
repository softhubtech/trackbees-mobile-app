import {LoadingController} from '@ionic/angular';
import {defaultHistoryDays} from '../../shared/consts';
import {TranslateService} from '@ngx-translate/core';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import * as Highcharts from 'highcharts';
import {Subject} from 'rxjs';
import {startWith, takeUntil} from 'rxjs/operators';
import {Hive, Position} from '../../model';
import {TraccarService} from '../../services/traccar/traccar.service';
import {DatePipe} from '@angular/common';
import {SegmentChangeEventDetail} from '@ionic/core';

@Component({
    selector: 'app-hive-data-history',
    templateUrl: './hive-data-history.page.html',
    styleUrls: ['./hive-data-history.page.scss'],
    providers: [DatePipe]
})
export class HiveDataHistoryPage implements OnInit, OnDestroy {
    hive: Hive;
    from = new Date();
    to = new Date();
    lastNDays = defaultHistoryDays;
    unsubscriber$ = new Subject();
    preloadedPositions;
    loadMessage: string;
    temperatureLineChart = [];
    humidityLineChart = [];
    chartsTime = [];
    temperatureLabel = '';
    humidityLabel = '';
    chartLabel = '';

    constructor(
        private router: Router,
        private traccarService: TraccarService,
        private datePipe: DatePipe,
        private translateService: TranslateService,
        private loadingCtrl: LoadingController
    ) {
    }

    ngOnInit(): void {
        this.hive = this.router.getCurrentNavigation().extras.state.hive;
        this.preloadedPositions = this.router.getCurrentNavigation().extras.state.positions;
        // Get Translated Label
        this.translateService
            .get([
                'TEMPERATURE_LABEL',
                'HUMIDITY_LABEL',
                'HISTORY_CHART_TITLE',
                'HIST_LOAD_MESSAGE'
            ])
            .pipe(takeUntil(this.unsubscriber$))
            .subscribe(value => {
                if (value.TEMPERATURE_LABEL) {
                    this.temperatureLabel = value.TEMPERATURE_LABEL;
                }
                if (value.HUMIDITY_LABEL) {
                    this.humidityLabel = value.HUMIDITY_LABEL;
                }
                if (value.HISTORY_CHART_TITLE) {
                    this.chartLabel = value.HISTORY_CHART_TITLE;
                }
                if (value.HIST_LOAD_MESSAGE) {
                    this.loadMessage = value.HIST_LOAD_MESSAGE;
                }
            });
    }

    ionViewDidEnter() {
        //  this.loadChart();
    }

    onRangeChange(event: CustomEvent<SegmentChangeEventDetail>) {
        const selectedValue = +event.detail.value;
        if (selectedValue) {
            this.lastNDays = selectedValue;
        }
        this.loadChartData(this.lastNDays);
    }

    loadChartData(numberOfDays: number) {
        this.from.setDate(this.from.getDate() - numberOfDays);
        const positions: Position[] =
            this.preloadedPositions !== undefined ? this.preloadedPositions : [];
        this.traccarService
            .devicePositions(this.hive, this.from, this.to)
            .pipe(
                takeUntil(this.unsubscriber$),
                startWith(positions)
            )
            .subscribe(positions2 => {
                positions2.forEach((position, index) => {
                    if (position.attributes.temperature) {
                        this.temperatureLineChart[index] = position.attributes.temperature;
                        this.chartsTime[index] = position.deviceTime;
                    }
                    if (position.attributes.humidity) {
                        this.humidityLineChart[index] = position.attributes.humidity;
                    }
                });
            });

        this.loadChart();
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    private loadChart() {
        const tempData = [];
        const humiData = [];

        this.chartsTime.forEach((data, i) => {
            const date = data.split('T')[0].split('-');
            const time = data
                .split('T')[1]
                .split('.')[0]
                .split(':');

            const y = Math.abs(date[0]);
            const m = Math.abs(date[1]) - 1;
            const d = Math.abs(date[2]);
            const h = Math.abs(time[0]);
            const n = Math.abs(time[1]);
            const s = Math.abs(time[2]);

            tempData.push([Date.UTC(y, m, d, h, n, s), this.temperatureLineChart[i]]);
            humiData.push([Date.UTC(y, m, d, h, n, s), this.humidityLineChart[i]]);
        });

        this.loadingCtrl.create().then(loading => {
            loading.present();
            Highcharts.chart('detailsChart', {
                chart: {
                    backgroundColor: 'rgb(248, 248, 254)',
                    type: 'spline'
                },
                title: {
                    text: this.hive.name,
                    style: {
                        fontFamily: 'var(--ion-font-family)',
                        fontSize: '16px',
                        fontWeight: '500'
                    },
                    align: 'center'
                },
                xAxis: {
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        day: '%e-%b',
                        minute: '%H:%M'
                    },
                    labels: {
                        overflow: 'justify'
                    },
                    gridLineWidth: 1,
                    gridLineDashStyle: 'Dash'
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                    minorGridLineWidth: 0,
                    gridLineWidth: 1,
                    alternateGridColor: null
                },
                plotOptions: {
                    spline: {
                        lineWidth: 2,
                        states: {
                            hover: {
                                lineWidth: 2
                            }
                        },
                        marker: {
                            // enabled: true
                        }
                    }
                },
                legend: {},
                credits: {enabled: false},
                series: [
                    {
                        name: this.temperatureLabel,
                        type: undefined,
                        data: tempData,
                        color: '#6BC8BF',
                        tooltip: {
                            valueSuffix: '°C'
                        }
                    },
                    {
                        name: this.humidityLabel,
                        type: undefined,
                        data: humiData,
                        color: '#FFC0CB',
                        tooltip: {
                            valueSuffix: '%'
                        }
                    }
                ],
                responsive: {
                    rules: [
                        {
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                plotOptions: {
                                    series: {
                                        marker: {
                                            radius: 2.5
                                        }
                                    }
                                }
                            }
                        }
                    ]
                }
            });
            loading.dismiss();
        });
    }
}
