import {Component, OnDestroy} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {TranslateService} from '@ngx-translate/core';

import {Router} from '@angular/router';
import {AuthenticationService} from './services/authentication/authentication.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AngularFirestore} from '@angular/fire/firestore';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent implements OnDestroy {
    private unsubscriber$ = new Subject();
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private translateService: TranslateService,
        private router: Router,
        private angularFirestore: AngularFirestore,
        private firebase: FirebaseX,
        private authenticationService: AuthenticationService) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.handleLandingPage();
        });
    }

    private handleLandingPage() {
        return this.authenticationService.isUserAuthenticated().pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(isUserAuthenticated => {
            if (!isUserAuthenticated) {
                this.router.navigateByUrl('welcome');
            } else {
                this.router.navigateByUrl('tabs/dashboard');
            }
            this.translateService.setDefaultLang('en');
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }


    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }
}
